import 'dart:convert';
import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_compaire/common_loader.dart';
import 'package:image_compaire/image_croping.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;
import 'package:compressimage/compressimage.dart';
import 'package:giffy_dialog/giffy_dialog.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:toast/toast.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
    File imageFile;
    File imageUpload1;
    File imageUpload2;
    bool isImageSelect1 = true;
    bool isImageSelect2 = true;
    bool isLoad = false;
    String imageName1;
    String imageName2;
    String Responce = "Response";
    var val = 0;
    bool isLoader1 = false;
    bool isLoader2 = false;
    final config = new ImageConfiguration();

    void showToast(String msg, {int duration, int gravity}) {
      Toast.show(msg, context, duration: duration, gravity: gravity);
    }
    
    editImage(File imageFile){
      ImageCropping.cropImage(imageFile)
          .then((File f) => setState(() {
        if (f != null){
          setState(() {
            if(val == 0) {
              imageUpload1 = f;
              isLoader1 = true;
              isImageSelect1 = false;
              registerAPI(f);
            }else {
              imageUpload2 = f;
              isLoader2 = true;
              isImageSelect2 = false;
              registerAPI(f);
            }
          });

        }else{
          return;
        }
      }));
    }

    getImage(int checkCamra) async {

      var image = await ImagePicker.pickImage(source: checkCamra == 0 ? ImageSource.gallery :ImageSource.camera, imageQuality: 70);
      try {

        imageFile = File(image.path);
        ImageCropping.cropImage(imageFile)
            .then((File f) => setState(() {
          if (f != null){
            setState(() {
              if(val == 0) {
                imageUpload1 = f;
                isImageSelect1 = false;
                isLoader1 = true;
                registerAPI(f);
              }else {
                isLoader2 = true;
                isImageSelect2 = false;
                imageUpload2 = f;
                registerAPI(f);
              }
            });

          }else{
            showToast("Image not found", gravity: Toast.BOTTOM);
            return;
          }
        }));
      } catch (e) {
        print(e);
      }

    }

    final spinkit =   SpinKitThreeBounce(color: Colors.white);


    registerAPI(File img) async {
      print("before"+img.lengthSync().toString());
       await CompressImage.compress(imageSrc: img.path, ); // desiredQuality: 30 desiredQuality ranges from 0 to 100
      print("After"+img.lengthSync().toString());
       setState(() { img; });
      var request = http.MultipartRequest('POST', Uri.parse("http://178.157.91.6/core/uploader.php"));
      request.files.add(await http.MultipartFile.fromPath(
            'file', img.path));
      http.Response response = await http.Response.fromStream(await request.send());
      if(response.body!=null){
        print("obje:${response.body}");
        if(val == 0){
          imageName1 = response.body;
        } else {
          imageName2 = response.body;
        }

      }else{
        showToast("Image not Uploaded", gravity: Toast.BOTTOM);
        if(val == 0){
          isImageSelect1 = true;
        } else {
          isImageSelect2 = true;
        }
        print("obje null");
        return null;
      }
      setState(() {
        isLoader1 = false;
        isLoader2 = false;
      });

    }



    compareImages(String img1,String img2,BuildContext context) async {
      String myUrl = "http://178.157.91.6/core/process.php";
      final response = await http.post(myUrl,
          headers: {'Accept': 'application/json'}, body: {'image_1': img1, 'image_2': img2});
      setState(() {
        isLoad = false;
      });
      if(response.body!=null){
        Map<String, dynamic> result = jsonDecode(response.body);
        showDialog(
            context: context,
            builder: (_) => AssetGiffyDialog(
              key: Key("Asset"),
              onlyOkButton: true,

              image: Image.asset(
                '${result['Overall']}' == "Pass" ? 'Images/Right.gif' : 'Images/Wrong.gif',
                fit: BoxFit.cover,
              ),
              title: Text(
                '${result['Overall']}' == "Pass" ? 'Success' : "Failed",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 22.0, fontWeight: FontWeight.w600),
              ),
              entryAnimation: EntryAnimation.DEFAULT,
              onOkButtonPressed: () {
                Navigator.pop(context);
              },
            ));

        setState(() {
          Responce = response.body;
        });
      }else{
        return null;
      }
    }


  @override
  Widget build(BuildContext context) {
    Widget selectImage() {
      showModalBottomSheet(
          context: context,
          builder: (BuildContext context) {
            return CupertinoActionSheet(
                title: const Text(
                  'Choose Options',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
                ),
                actions: <Widget>[
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(20),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            InkWell(
                              onTap: () async{
                                Navigator.pop(context);
                              await getImage(0);
                              },
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    width: 50,
                                    height: 50,
                                    decoration: BoxDecoration(
                                        color: Colors.black.withOpacity(0.6),
                                        shape: BoxShape.circle),
                                    child: Icon(
                                      Icons.perm_media,
                                      color: Colors.white,
                                      size: 20,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "Gallery",
                                    style: TextStyle(
                                      color: Colors.black.withOpacity(0.7),
                                      fontSize: 18,
                                    ),
                                  )
                                ],
                              ),
                            ),
                            InkWell(
                              onTap: () async{
                                Navigator.pop(context);
                                await getImage(1);
                              },
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    width: 50,
                                    height: 50,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.black.withOpacity(0.6)),
                                    child: Icon(
                                      Icons.camera_alt,
                                      color: Colors.white,
                                      size: 20,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "Camera",
                                    style: TextStyle(
                                      color: Colors.black.withOpacity(0.7),
                                      fontSize: 18,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Divider(
                        height: 0.5,
                        color: Colors.black.withOpacity(0.5),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Center(
                          child: Text(
                            "Cancel",
                            style: TextStyle(color: Colors.black, fontSize: 20),
                          ),
                        ),
                      ),
                    ],
                  )
                ]);
          });
    }



    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
        Image.asset(
        'Images/omantel_logo.jpg',
        fit: BoxFit.contain,
        height: 72,
    ),
    ],
      ),
      ),
      body: ListView(
        padding: EdgeInsets.all(30),
          children: <Widget>[
            DottedBorder(
               color: Colors.black,
               strokeWidth: 1,
               borderType: BorderType.RRect,
               radius: Radius.circular(15),
               child: Container(
                 height: 200,
                 width:  MediaQuery.of(context).size.width-60,
                 decoration: new BoxDecoration(
                   shape: BoxShape.rectangle,
                   borderRadius:
                   new BorderRadius.all(
                       Radius.circular(4)),
                 ),
                 child:  Stack(
                   children: <Widget>[
                     InkWell(
                         child: Center(child: isImageSelect1 ? Container(child: Image.asset('Images/id-card.png',width: 80,height: 80)) : Container(
//                             width: double.infinity,
                             decoration: BoxDecoration(
                               borderRadius: BorderRadius.circular(12),
                               border: Border.all(
                                 color: Colors.black,
                                 width: 6,
                               ),
                            ),

                             child: Image.file(imageUpload1,fit: BoxFit.fitWidth,))),onTap: (){
                         if(isImageSelect1) {
                           setState(() {
                                     val = 0;
                                });
                            selectImage();
                            print("tap");
                           }
                     },),
                     isLoader1  ? spinkit :Container(),
                     Positioned(
                       top: 5,
                       right: 10,
                       child: isImageSelect1 ?  Container(): InkWell(child: Icon(Icons.edit,color: Colors.red,),onTap: (){
                         setState(() {
                           Responce = "Response";
                           val = 0;
//                           isLoader2 = true;
                         });
                         editImage(imageUpload1);
                       },),),
                     Positioned(
                       top: 40,
                       right: 10,
                       child: isImageSelect1 ?  Container(): InkWell(child: Icon(Icons.delete,color: Colors.red,),onTap: (){
                         setState(() {
                           Responce = "Response";
                           imageName1 = "";
                           isImageSelect1 = true;
                           isLoader2 = false;
                           isLoader1 = false;
                         });
                       },),),
                   ],
                 ),
               ),
             ),
            SizedBox(height: 30,),
            DottedBorder(
              color: Colors.black,
              strokeWidth: 1,
              borderType: BorderType.RRect,
              radius: Radius.circular(15),
              child: Container(
                height: 200,
                width:  MediaQuery.of(context).size.width-60,
                decoration: new BoxDecoration(
                  shape: BoxShape.rectangle,
                  borderRadius:
                  new BorderRadius.all(
                      Radius.circular(4)),
                ),
                child:  Stack(
                  children: <Widget>[
                    InkWell(
                        child: Center(child: isImageSelect2 ? Container(child: Image.asset('Images/live-tracker.png',width: 80,height: 80,)) : Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              border: Border.all(
                                color: Colors.black,
                                width: 6,
                              ),
                            ),
//                            width: double.infinity,
                            child: Image.file(imageUpload2,fit: BoxFit.fitWidth,))),onTap: (){
                         if(isImageSelect2) {
                           setState(() {
                             val = 1;
                           });
                           selectImage();
                         }
                    }),
                    isLoader2  ? spinkit :Container(),

                    Positioned(
                      top: 5,
                      right: 10,
                      child: isImageSelect2 ?  Container(): InkWell(child: Icon(Icons.edit,color: Colors.red),onTap: (){
                        setState(() {
                          Responce = "Response";
                          val = 1;
                        });
                        editImage(imageUpload2);
                      },),),
                    Positioned(
                      top: 40,
                      right: 10,
                      child: isImageSelect2 ?  Container(): InkWell(child: Icon(Icons.delete,color: Colors.red),onTap: (){
                        setState(() {
                          Responce = "Response";
                          imageName2 = "";
                          isImageSelect2 = true;
                          isLoader2 = false;
                          isLoader1 = false;
                        });
                      },),),
                  ],
                ),
              ),
            ),
            SizedBox(height: 30,),
            isLoad ? CommonLoader() : RaisedButton(child: Text("Compare",style: TextStyle(fontSize: 22),),
              onPressed: (){
              setState(() {
                isLoad = true;
              });               
                    print("imageUpload1:${imageName1}");
                    print("imageUpload2:${imageName2}");
                     if(imageName1 != "" && imageName1 != null && imageName2 != null && imageName2 != "") {
                       compareImages(imageName1, imageName2,context);
                     }else{
                       setState(() {
                         isLoad = false;
                       });
                     }
              },
              color: Colors.white,
              textColor: Colors.blue,
              padding: EdgeInsets.fromLTRB(100, 15, 100, 15),
              splashColor: Colors.grey,
            ),
            SizedBox(height: 30,),
            DottedBorder(
              color: Colors.black,
              strokeWidth: 1,
              borderType: BorderType.RRect,
              radius: Radius.circular(15),
              child: Container(
                height: 100,
                width:  MediaQuery.of(context).size.width-60,
                decoration: new BoxDecoration(
                  shape: BoxShape.rectangle,
                  borderRadius:
                  new BorderRadius.all(
                      Radius.circular(4)),
                ),
                child: Center(child: SingleChildScrollView(
                    child: Text("$Responce"))),
              ),
            ),
          ],
        ),
      );
  }

  
}
