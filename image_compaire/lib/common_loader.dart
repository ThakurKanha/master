import 'package:flutter/material.dart';

const double small = 15;
const double reg = 35;

class CommonLoader extends StatelessWidget {
  final double size;

  CommonLoader({size: reg}) : this.size = size ?? reg;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        height: size,
        width: size,
        child: CircularProgressIndicator(
          strokeWidth: size == small ? 1.5:2,
          valueColor: AlwaysStoppedAnimation<Color>(
            Theme.of(context).accentColor,
          ),
        ),
      ),
    );
  }
}
